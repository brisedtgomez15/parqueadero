/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parqueadero;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Matias
 */
public class Garaje {

    List<Coche> coches;

    public Garaje() {
        coches = new LinkedList();

    }

    public Coche buscarCoche(Coche uno) {
        for (Coche dato : coches) {
            if (dato.esIgual(uno)) {
                return dato;
            }
        }
        return null;
    }

    public List getCoches() {
        return coches;
    }

    public void addCoche(Coche azul) {
        coches.add(azul);
    }

    public LinkedList<String> palabraClave(String palab) {
        LinkedList<String> l = new LinkedList();
        for (Coche d : coches) {
            LinkedList clave = d.buscarReparaciones(palab);
            String conk = "";
            if (clave.size() > 0) {
                conk += "Matricula correpondiente: " + d.getMatricula() + " Direccion correpondiente: " + d.getDireccionDueno();
                for (Object c : clave) {
                    conk += "\n -" + c.toString();
                }
                l.add(conk);
            }
        }
        return l;

    }

}
