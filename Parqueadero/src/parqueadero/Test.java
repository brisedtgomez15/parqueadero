/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parqueadero;

import java.util.*;

public class Test {

    private static Garaje garaje = new Garaje();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String matricula = "";
        while (!matricula.toLowerCase().equals("no")) {
            System.out.println("Digite una matricula de auto, si no desea ingresar más vehiculos digite 'no'");
            matricula = sc.next();
            if (!matricula.toLowerCase().equals("no")) {
                System.out.println("digite dirección");
                String direccion = sc.next();
                Coche c = new Coche(direccion, matricula);
                Coche g = garaje.buscarCoche(c);
                String descripcion = "";
                int km = 0;
                System.out.println("descripción");
                descripcion = sc.next() + " " + sc.nextLine();
                System.out.println("Kilometraje: ");
                km = sc.nextInt();
                if (g != null) {
                    g.addReparacion(descripcion, km);
                } else {

                    c.addReparacion(descripcion, km);
                    garaje.addCoche(c);
                }

            }

        }
        System.out.println("Digite la palabra clave: ");
        String clave = sc.next();
        LinkedList<String> a = garaje.palabraClave(clave);
        System.out.println(garaje.getCoches());
        System.out.println("\n\n\n----- BUSQUEDA DE PALABRAS CLAVE -----");
        for (int i = 0; i < a.size(); i++) {
            System.out.println(i+1+". "+a.get(i));
        }

    }
}
