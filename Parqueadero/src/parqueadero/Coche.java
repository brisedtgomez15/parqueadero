/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parqueadero;

import java.util.LinkedList;
import java.util.List;
import javax.swing.JOptionPane;

public class Coche {

    private String direccionDueno;
    private String matricula;
    LinkedList<Reparacion> reparaciones;

    public Coche(String direccionDueno, String matricula) {
        this.direccionDueno = direccionDueno;
        this.matricula = matricula;
        reparaciones = new LinkedList();
    }

    public String getDireccionDueno() {
        return direccionDueno;
    }

    public void setDireccionDueno(String direccionDueno) {
        this.direccionDueno = direccionDueno;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public void addReparacion(String descripcion, int kms) {
        ///deben tener en cuenta que la reparación que se va agregar los kilometros no se han inferior a la reparación anterior
        Reparacion dos = new Reparacion(descripcion, kms);
        if (reparaciones.size() > 0) {
            if (reparaciones.get(reparaciones.size() - 1).getKms() < kms) {
                reparaciones.add(dos);
            } else {
                JOptionPane.showMessageDialog(null, "Kilometraje invalido");
            }

        } else {
            reparaciones.add(dos);
        }

    }

    public LinkedList buscarReparaciones(String plabraClave) {
        LinkedList<Reparacion> reparacionesEspecificas = new LinkedList();
        for (Reparacion rep : reparaciones) {
            if (rep.getDescripcion().contains(plabraClave)) {
                reparacionesEspecificas.add(rep);
            }

        }
        ////iterar Iterador, for, while......
        /////String tiene metodos para determinar si un texto contiene una plabra.
        return reparacionesEspecificas;

    }

    public Reparacion buscarUltimaReparación() {
        Reparacion reparacion = null;
        reparacion = reparaciones.getLast();

        ///linkedList tiene un metodo que le permite obtener el ultimo elemento registrado
        return reparacion;
    }

    public boolean esIgual(Coche coche) {
        if (coche.getDireccionDueno().equals(this.direccionDueno) && coche.getMatricula().equals(this.matricula)) {

            return true;
        }
        return false;
    }

    public String toString() {
        String re = "";
        for (Reparacion rep : reparaciones) {
            re += "       -" + rep + "\n";
        }
        return "\n\nMatricula: " + matricula + "\nDireccion dueño: " + direccionDueno + "\n       Reparaciones:\n" + re + "\n        Ultima registrada: " + buscarUltimaReparación();
    }

}
